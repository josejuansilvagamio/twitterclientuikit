//
//  ListPeopleTableViewCell.swift
//  TwitterClientUIKit
//
//  Created by Jose Juan Silva Gamino on 09/01/22.
//

import UIKit

class ListPeopleTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
