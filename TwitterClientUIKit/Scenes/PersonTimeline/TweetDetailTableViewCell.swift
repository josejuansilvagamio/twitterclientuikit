//
//  TweetDetailTableViewCell.swift
//  TwitterClientUIKit
//
//  Created by Jose Juan Silva Gamino on 30/01/22.
//

import UIKit

class TweetDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var teetDateLabel: UILabel!
    @IBOutlet weak var tweetDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
