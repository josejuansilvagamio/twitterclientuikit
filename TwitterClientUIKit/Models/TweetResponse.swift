//
//  TweetResponse.swift
//  TwitterClientUIKit
//
//  Created by Jose Juan Silva Gamino on 10/01/22.
//

import Foundation

struct TweetResponse : Codable{
    let data : [TweetDetail]
}
