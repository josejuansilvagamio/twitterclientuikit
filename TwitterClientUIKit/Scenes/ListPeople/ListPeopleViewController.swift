//
//  ListPeopleViewController.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 03/01/22.
//

import UIKit


class ListPeopleViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel : ListPeopleViewModelProtocol
    var coordinator : MainCoordinator
    
    init(nibNameOrNil: String, viewModel: ListPeopleViewModelProtocol, coordinator: MainCoordinator){
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nibNameOrNil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "ListPeopleTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "listPeopleTableviewCell")
        self.title = "List people"
        Task.init{
            try await viewModel.fetchUsers()
            tableView.reloadData()
        }
    }
}

extension ListPeopleViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "listPeopleTableviewCell" , for: indexPath) as? ListPeopleTableViewCell else {
            return UITableViewCell()
        }
        let user = viewModel.getUserFor(index: indexPath.row)
        cell.nameLabel.text = user.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = viewModel.getUserFor(index: indexPath.row)
        return coordinator.goToTimeline(twitterUser: user)
    }
}
