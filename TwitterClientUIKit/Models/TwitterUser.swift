//
//  TwitterUser.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 01/01/22.
//

import Foundation

struct TwitterUser: Codable{
    let id : String
    let name : String
    let username : String
}

