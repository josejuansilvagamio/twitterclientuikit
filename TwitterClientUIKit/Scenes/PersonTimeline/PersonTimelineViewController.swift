//
//  PersonTimelineViewController.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 23/12/21.
//

import UIKit
import Foundation
import Combine

class PersonTimelineViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: PersonTimelineViewModelProtocol?
    var coordinator: MainCoordinator?
   
    init(nibName nibNameOrNil: String?, viewModel: PersonTimelineViewModelProtocol, coordinator: MainCoordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nibNameOrNil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.viewModel = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TweetDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "TweetDetailTableViewCell")
        Task.init{
            let _ = try await viewModel?.fetchTimeline()
            tableView.reloadData()
        }
        
    }
}

extension PersonTimelineViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TweetDetailTableViewCell", for: indexPath) as? TweetDetailTableViewCell else {
            return UITableViewCell()
        }
        let tweet = viewModel?.getTweetFor(index: indexPath.row)
        cell.tweetDetailLabel.text = tweet?.text ?? ""
        cell.teetDateLabel.text = tweet?.created_at ?? ""
        return cell
    }
}
