//
//  TwitterClientNetworking.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 23/12/21.
//

import Foundation
import Combine

//Twitter keys
let APIKey = "1dcp8RciP5FbbVWzqkReueYWB"
let APIKeySecret = "Juc2rGc5qtaYbQsUntDEHhFM0RVz6RncWSwGm5B4k13HtqODPX"
let BearerToken = "AAAAAAAAAAAAAAAAAAAAALoFXwEAAAAAm0O92rcbv5%2BzA%2FwQ16oOVjwl5es%3DOaHzJHvm99MAr0QF2ROEhzmiGFfzTj31pgTer0ltAbqjvdIbew"

protocol TwitterClientNetworkingProtocol{
    func fetchUserBy(userName: String) async throws -> TwitterUser
    func fetchTimeline(for userID : String) async throws -> TweetResponse
}

struct TwitterClientNetworking: TwitterClientNetworkingProtocol{
    func fetchUserBy(userName: String) async throws -> TwitterUser{
        guard let twitterURL = URL(string: "https://api.twitter.com/2/users/by/username/\(userName)") else{
            throw NetworkingError.invalidURL
        }
        var request = URLRequest(url: twitterURL)
        request.setValue("Bearer \(BearerToken)", forHTTPHeaderField: "Authorization")
        guard let (data, response) = try? await URLSession.shared.data(for: request) else{
            throw NetworkingError.requestError
        }
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else{
            throw NetworkingError.responseStatusNotOK
        }

        guard let result = try? JSONDecoder().decode(TwitterUserResponse.self, from: data) else{
            throw NetworkingError.decodingError
        }
        return result.data
    }
    
    
    func fetchTimeline(for userID : String) async throws -> TweetResponse{
        guard let twitterURL = URL(string: "https://api.twitter.com/2/users/\(userID)/tweets?tweet.fields=created_at&expansions=author_id&user.fields=created_at&max_results=5") else{
            throw NetworkingError.invalidURL
        }
        var request = URLRequest(url: twitterURL)
        request.setValue("Bearer \(BearerToken)", forHTTPHeaderField: "Authorization")
        guard let (data, response) = try? await URLSession.shared.data(for: request) else{
            throw NetworkingError.requestError
        }
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else{
            throw NetworkingError.responseStatusNotOK
        }

        guard let result = try? JSONDecoder().decode(TweetResponse.self, from: data) else{
            throw NetworkingError.decodingError
        }
       return result
    }
}


enum NetworkingError: Error{
    case invalidURL, requestError, decodingError, responseStatusNotOK
}










