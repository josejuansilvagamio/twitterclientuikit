//
//  Coordinator.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 02/01/22.
//

import Foundation
import UIKit

protocol Coordinator{
    var children : [Coordinator] {get set}
    var navigationController : UINavigationController {get set}
    func start()
}

class MainCoordinator : Coordinator{
    var children = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        let listPeopleViewModel = ListPeopleViewModel(networking: TwitterClientNetworking())
        let listPeopleViewController = ListPeopleViewController(nibNameOrNil: "ListPeopleViewController", viewModel: listPeopleViewModel, coordinator: self)
        navigationController.pushViewController(listPeopleViewController, animated: true)
    }
    
    func goToTimeline(twitterUser: TwitterUser){
        let personTimelineViewModel = PersonTimelineViewModel(user: twitterUser, networking: TwitterClientNetworking())
        let initialViewController = PersonTimelineViewController(nibName: "PersonTimelineViewController", viewModel: personTimelineViewModel, coordinator: self)
        navigationController.pushViewController(initialViewController, animated: true)
    }
}
