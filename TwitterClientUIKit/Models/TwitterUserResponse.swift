//
//  TwitterUserResponse.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 01/01/22.
//

import Foundation
struct TwitterUserResponse : Codable{
    let data : TwitterUser
}
