//
//  PersonTimelineViewModel.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 23/12/21.
//

import Foundation

protocol PersonTimelineViewModelProtocol{
    func fetchUserBy(userName: String) async throws -> TwitterUser
    func fetchTimeline() async throws -> TweetResponse
    func getTweetFor(index: Int)->TweetDetail?
    func getNumberOfRows()->Int
}

class PersonTimelineViewModel : PersonTimelineViewModelProtocol{
    private var tweets : TweetResponse?
    private var user : TwitterUser
    private var networking : TwitterClientNetworkingProtocol
    init(user: TwitterUser, networking: TwitterClientNetworkingProtocol){
        self.user = user
        self.networking = networking
    }
    
    func fetchTimeline() async throws -> TweetResponse {
        let networking = TwitterClientNetworking()
        let timeline = try await networking.fetchTimeline(for: user.id)
        self.tweets = timeline
        return timeline
    }
    
    func fetchUserBy(userName: String) async throws -> TwitterUser{
        let user = try await networking.fetchUserBy(userName: "")
        return user
    }
    
    func getNumberOfRows()->Int{
        tweets?.data.count ?? 0
    }
    
    func getTweetFor(index: Int)->TweetDetail?{
        return tweets?.data[index]
    }
}
