//
//  TweetDetail.swift
//  TwitterClientUIKit
//
//  Created by Jose Juan Silva Gamino on 10/01/22.
//

import Foundation

struct TweetDetail: Codable{
    let id : String
    let text : String
    let created_at : String
}
