//
//  ListPeopleViewModel.swift
//  TwitterClientUIKit
//
//  Created by jose juan silva gamiño on 03/01/22.
//

import Foundation

protocol ListPeopleViewModelProtocol {
    func fetchUserBy(userName: String) async throws -> TwitterUser
    func getNumberOfRows()->Int
    func getUserFor(index: Int)->TwitterUser
    func fetchUsers() async throws
}

class ListPeopleViewModel : ListPeopleViewModelProtocol{
    private var twitterUsers = [TwitterUser]()
    private var networking : TwitterClientNetworkingProtocol
    private var twitterUsersNames = ["josejuansilva", "lopezdoriga", "TwitterDev", "johnsundell", "_inside", "clattner_llvm", "twostraws"]

    init(networking: TwitterClientNetworkingProtocol ){
        self.networking = networking
    }
    
    func fetchUsers() async throws {
        self.twitterUsers = try await fetchTwitterUsers()
    }
    
    func fetchUserBy(userName: String) async throws -> TwitterUser {
        let user = try await networking.fetchUserBy(userName: userName)
        return user
    }
    
    func fetchTwitterUsers() async throws -> [TwitterUser] {
        var users = [TwitterUser]()
        let networking = TwitterClientNetworking()
        for name in twitterUsersNames{
            let user = try await networking.fetchUserBy(userName: name)
            users.append(user)
        }
        return users
    }
    
    func getNumberOfRows()->Int{
        twitterUsers.count
    }
    
    func getUserFor(index: Int)->TwitterUser{
        return twitterUsers[index]
    }
}

